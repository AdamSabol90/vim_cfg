return {
	"mfussenegger/nvim-lint",
	event = { "BufReadPre", "BufNewFile" },
	config = function()
		local lint = require("lint")

		-- Define linters by file type
		lint.linters_by_ft = {
			javascript = { "eslint_d" },
			typescript = { "eslint_d" },
			javascriptreact = { "eslint_d" },
			typescriptreact = { "eslint_d" },
			svelte = { "eslint_d" },
			python = { "pylint" },
		}

		local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })

		-- Add state to track whether linting is enabled
		local isLintingEnabled = true

		-- Toggle function for linting
		local function toggleLinting()
			isLintingEnabled = not isLintingEnabled
			if not isLintingEnabled then
				vim.diagnostic.hide()
			else
				lint.try_lint()
			end
		end

		-- Autocommands for triggering linting based on state
		vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
			group = lint_augroup,
			callback = function()
				if isLintingEnabled then
					lint.try_lint()
				end
			end,
		})

		-- Keymaps
		vim.keymap.set("n", "<leader>l", function()
			lint.try_lint()
		end, { desc = "Trigger linting for current file" })

		vim.keymap.set("n", "<leader>tl", toggleLinting, { desc = "Toggle linting on and off" })
	end,
}
