return {
    "jose-elias-alvarez/null-ls.nvim",
    priority = 500, -- Set the priority according to your loading preference
    config = function()
        local null_ls = require("null-ls")
        null_ls.setup({
            sources = {
                null_ls.builtins.formatting.sqlfluff.with({
                    extra_args = { "--dialect", "bigquery" } -- Adjust as needed
                }),
                null_ls.builtins.diagnostics.sqlfluff.with({
                    extra_args = { "--dialect", "bigquery" }
                }),
            },
        })
    end,
    ft = { "sql" }, -- Lazy load for SQL file types
}
