return {
  'mrcjkb/rustaceanvim',
  version = '^4', -- Recommended
  lazy = false, -- This plugin is already lazy

  config = function()
    vim.g.rustaceanvim = {
      tools = {
        -- Configuration for Rustacean tools
      },
      server = {
        on_attach = function(client, bufnr)
          -- Buffer local mappings and functions
          local keymap = vim.keymap
          local opts = { buffer = bufnr, silent = true }

          -- Example: Set up keybindings as per LSP
          keymap.set("n", "gR", "<cmd>Telescope lsp_references<CR>", opts)
          keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<CR>", opts)
          -- Additional keymaps...

          -- Function to toggle diagnostics
          local diagnostic_enabled = true
          local function toggle_diagnostics()
            diagnostic_enabled = not diagnostic_enabled
            if diagnostic_enabled then
              vim.diagnostic.show(nil, bufnr)
            else
              vim.diagnostic.hide(nil, bufnr)
            end
          end

          keymap.set("n", "<leader>td", toggle_diagnostics, { desc = "Toggle diagnostics on/off", buffer = bufnr })

          -- Automatically set up upon LSP attach
          vim.api.nvim_create_autocmd("LspAttach", {
            group = vim.api.nvim_create_augroup("UserLspConfig", {}),
            callback = function(ev)
              -- Additional event-driven configurations
            end,
          })
        end,
        default_settings = {
          ['rust-analyzer'] = {
            -- rust-analyzer specific settings
          },
        },
      },
    dap = {
        -- Integration with nvim-dap, dap-ui, and possibly dap-virtual-text
        setup = function()
          local dap = require('dap')
          local dapui = require('dapui')

          dapui.setup()  -- Setup DAP UI

          -- Example: Define how dap sessions start and end
          dap.listeners.after.event_initialized["dapui_config"] = function()
            dapui.open({})
          end
          dap.listeners.before.event_terminated["dapui_config"] = function()
            dapui.close({})
          end
          dap.listeners.before.event_exited["dapui_config"] = function()
            dapui.close({})
          end

          -- Keybindings for dap
          local keymap = vim.keymap.set
          keymap('n', '<leader>db', dap.toggle_breakpoint, {desc = "Toggle Breakpoint"})
          keymap('n', '<leader>dc', dap.continue, {desc = "Continue"})
          -- Additional dap key mappings...
        end,
        ensure_installed = {
          -- Ensure necessary debuggers are installed, customize as needed
        },
      },
    }
  end,
}
