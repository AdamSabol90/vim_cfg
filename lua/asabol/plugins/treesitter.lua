-- Treesitter configuration
return {
  "nvim-treesitter/nvim-treesitter",
  event = { "BufReadPre", "BufNewFile" },
  build = ":TSUpdate",
  dependencies = {
    "windwp/nvim-ts-autotag",
  },
  config = function()
    -- Import nvim-treesitter plugin
    local treesitter = require("nvim-treesitter.configs")

    -- Add gotmpl parser configuration
    local parser_config = require'nvim-treesitter.parsers'.get_parser_configs()
    parser_config.gotmpl = {
      install_info = {
        url = "https://github.com/ngalaiko/tree-sitter-go-template",
        files = {"src/parser.c"}
      },
      filetype = "gotmpl",
      used_by = {"gohtmltmpl", "gotexttmpl", "gotmpl", "yaml"}
    }

    -- Configure Treesitter
    treesitter.setup({
      highlight = { enable = true },
      indent = { enable = true },
      autotag = { enable = true },
      ensure_installed = {
        "json", "javascript", "c_sharp", "bash", "go", "rust",
        "python", "sql", "typescript", "tsx", "yaml", "html", "css",
        "prisma", "helm", "markdown", "markdown_inline", "svelte",
        "graphql", "bash", "lua", "vim", "dockerfile", "gitignore",
        "query", "vimdoc", "c", "gotmpl" -- Add gotmpl to the installed parsers
      },
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "<C-space>",
          node_incremental = "<C-space>",
          scope_incremental = false,
          node_decremental = "<bs>",
        },
      },
    })
  end,
}
